\documentclass[12pt,letterpaper]{article}
\input{Preamble.tex}

\title{Capstone Proposal}
\author{Caleb Reister}

% Graphics configuration
\DeclareGraphicsExtensions{.pdf,.png,.jpg}

\begin{document}
\maketitle
\tableofcontents

\clearpage
\section{Introduction}
When searching for a project, three general guidelines were kept in mind:
\begin{enumerate}
    \item Demonstrate mastery of both analog and digital electronics
    \item Digital Signal Processing should play an important role in the project
    \item The project should utilize modern design and construction techniques
\end{enumerate}

To attain these goals, I propose designing, building, and testing a digitally programmable precision integrating ADC front-end. The analog portion of the design will include a high-impedance differential input, reference generator circuits, integrators, and sigma-delta modulators. All of the analog circuits will be optimized for high-precision, low-noise, and low-distortion measurements required for audio, industrial, and laboratory applications.

All of the conversion logic will be implemented in an FPGA, and an embedded processor will be used to report conversion results and configure the analog circuits. Configuration will be performed through a serial port, with the exception of control signals used during conversions.

\section{Goals}
Since this project can easily be expanded, scope creep may become an issue. It is likely that the PCB will be \textit{capable} of implementing more ADC architectures than can implemented digitally in a reasonable amount of time. Thus, it is essential that the basic functionality of the system is demonstrated before adding more advanced architectures.

\subsection{Required}
\begin{itemize}
    \item Design a PCB containing the analog hardware necessary to implement several different ADC architectures.
    \item Implement the digital portion of at least two ADC architectures (Dual-Slope and Sigma-Delta) in an FPGA.
    \item Program an embedded processor to configure and acquire samples from the ADC architectures.
\end{itemize}

\subsection{Desired}
\begin{itemize}
    \item Add a low-resolution ADC and DAC to the PCB design to enable the creation of more advanced Multi-Slope and Sigma-Delta architectures.
    \item Implement additional ADC architectures, such as SVFC (Synchronous Voltage-to-Frequency Converter) and SAR (Successive Approximation)
    \item Implement a simple Linux driver and GUI to configure and visualize the ADC data in real time using an embedded hard processor.
\end{itemize}

\subsection{Stretch}
\begin{itemize}
    \item Implement an AXI (Advanced eXtensible Interface) or APB (Advanced Peripheral Bus) interface for each ADC architecture to enable configuration of the digital hardware using an embedded processor.
    \item Add a configurable anti-aliasing filter stage to the PCB
    \item Add automatic calibration functionality to compensate for DC offsets and drift introduced in the analog stage.
    \item Push the limits of the analog hardware in order to determine ADC characteristics such as ENOB (Effective Number of Bits) and SINAD (Signal to Noise and Distortion Ratio).
\end{itemize}

\section{Deliverables}
\begin{description}
    \item[Hardware] the completed analog front-end PCB will adhere to the \href{https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ds/hsmc_spec.pdf}{HSMC specification} and contain all of the components necessary to implement a variety of ADC architectures.
    \item[Source code] for the logic that implements the ADC architectures, and any software written to test, configure, and acquire data from the PCB or FPGA fabric. 
    \item[Documentation] the Capstone Report will contain all of the design documentation and source code for the project. It may be supplemented with additional source code documentation.
\end{description}

The software, HDL code, and PCB designs developed for this project may be distributed under a permissive open-source license in order to allow individuals and corporations to use, modify, and expand the project without being required to disclose proprietary information.

\section{System Design}
Figures \ref{fig:block:main}--\ref{fig:block:sigmadelta} show a top-level block diagram of the analog front-end PCB. By enabling or disabling certain components, the board can be configured to perform conversions using a wide variety of conversion techniques.

\begin{figure}
    \includegraphics[width=\textwidth]{./BlockDesign/Main.pdf}
    \caption{block diagram of the analog front-end}
    \label{fig:block:main}
\end{figure}

\begin{figure}
    \includegraphics[width=\textwidth]{./BlockDesign/MultiSlope.pdf}
    \caption{Dual-Slope configuration}
    \label{fig:block:multislope}
\end{figure}

\begin{figure}
    \includegraphics[width=\textwidth]{./BlockDesign/SigmaDelta2.pdf}
    \caption{2nd-order Sigma-Delta configuration}
    \label{fig:block:sigmadelta}
\end{figure}

\section{Budget}
\begin{tabularx}{\textwidth}{>{\raggedright}p{1in} c X}
  \textbf{Item(s)} & \textbf{Cost} & \textbf{Description} \\
  \hline
  \endhead
  Input Stage Components & $\$40-\$80$ & The input stage will consist of a high-performance instrumentation amplifier with programmable gain, plus some additional support and protection circuitry.\\
  \hline
  Integration Stage Components & $\$20-\$40$ & This stage will contain configurable integrators, difference amplifiers, and analog multiplexers.\\
  \hline
  Voltage Regulators \& References & $\$20-\$40$ & A 10V linear voltage regulator will be used to power all of the analog circuits. In addition, several buffered precision reference circuits will be necessary.\\
  % \begin{itemize}
  %    \item A low-impedance virtual ground circuit
  %    \item An adjustable precision voltage reference with level-shifting circuits for the dual-slope and Σ-Δ architectures
  %    \item A precision current source for the SVFC
  % \end{itemize}\\
  \hline
  Connectors \& Adapters & $\$20-\$40$ & One high-performance HSMC connector will be required to interface between the FPGA and analog hardware. In addition, a probe connector will be necessary for the input stage.\\
  \hline
  PCB Fabrication Service & $\$20-\$40$ & The PCB will require at least four layers and be approximately \SI{3}{in} wide.\\
  \hline
  Passive Components & $\$20-\$40$ & With a few exceptions, resistors and capacitors will be of ``average'' quality. Most of the analog ICs will need to be heavily decoupled, and the input stage may be shielded.\\
  \hline
  \textbf{Total} & $\$550-\$1,240$ & Cost estimates do not include shipping and additional fees. Some components may also have quantity discounts.
\end{tabularx}

\section{Dependencies}
In addition to the components outlined in the budget, a variety of equipment will be required. All of the necessary equipment should be available on-campus.
\begin{itemize}
    \item High-quality soldering and reflow equipment to assemble and inspect the PCB
    \item Mixed-signal oscilloscope and function generator to test the PCB
    \item Spectrum analyzer to establish a noise floor
\end{itemize}

\section{Risk Analysis}
Since the goal is to demonstrate mastery of both digital and analog design, this is a somewhat ambitious project. Since the ADC architectures in question have all been implemented before, there is little technical risk. However, there are some significant logistical risks.

\subsection{Time}
In order to maintain the deadlines, a strict schedule must be followed. However, it is likely that some tasks will take longer than expected, so there needs to be enough flexibility to account for unforeseen problems. For instance, if the PCB has an issue, a significant amount of rework and debug time will be required.

\subsection{Cost}
The project will be personally funded. If external costs are higher than expected, the project budget may need to be reduced significantly. In addition, precision analog components can be very expensive. If component costs begin to skyrocket, precision will be sacrificed in order to meet the budget requirements.

\section{Schedule}
\includegraphics[width=\textwidth]{Schedule.pdf}

\end{document}

%%% Local Variables:
%%% TeX-master: t
%%% mode: luatex
%%% TeX-engine: luatex
%%% End:
