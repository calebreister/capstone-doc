\graphicspath{{./img/Design_AnalogIntSum/}}
\chapter{Integration \& Summing Stage} \label{design:intsum}
The integration and summing (\textit{IntSum}) stages can be considered the ``core'' of the analog design. At a basic level, each stage incorporates an op-amp integrator and an (optional) differential amplifier, allowing each stage to be used for both integration and delta modulation. Although the idea is simple, the implementation of a configurable analog stage that operates correctly over a wide range of frequencies is not.

\section{Specification} \label{design:intsum:spec}
\begin{itemize}[noitemsep]
    \item Direct integration and sigma-delta modes
    \item Selectable input and reference voltages
    \item Adjustable integrator gain
    \item Fast integrator reset
    \item High impedance disable state (with integrator held in reset)
    \item Capable of integrating a DC signal for up to \SI{20}{ms}
    \item High slew rate for sigma-delta modulation
\end{itemize}

\subsection{Constraints} \label{design:intsum:const}
The requirements placed on the integrator in dual-slope and sigma-delta mode are somewhat conflicting. Dual-slope conversions work best at low frequencies, and have excellent power supply rejection. Although DC offsets are canceled during the integration process, saturation can still occur at low frequencies, on the other hand, sigma-delta ADCs require a high oversampling frequency, and the modulated reference signal must have sharp edges. This low offset, high slew rate requirement limits op-amp options.

Feedback capacitor selection for the integrator is another consideration. Using an analog switch to select integration capacitors is not an option, since even a \SI{1}{\ohm} on resistance would have a detrimental effect on performance. Thus, a single value that allows for both low- and high-speed operation through resistor selection must be chosen.

\FloatBarrier
\section{Integrator} \label{design:intsum:int}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.75\textwidth]{int-sch.pdf}
    \caption{integrator with reset logic and slope selection}
    \label{fig:design:intsum:int-sch}
\end{figure}

Each integrator includes a reset switch and can be configured to use one of four input resistors. Figure \ref{fig:design:intsum:int-sch} shows the final schematic for one of the integrators. Due to the highly sensitive nature of integrators, multiple error sources were taken into account.

\begin{description}
    \item[Op-Amp] selection has the most significant impact on integrator performance. The lack of a DC feedback path most of the time means that offsets must be low enough to prevent saturation, but it must also respond quickly enough to minimize distortion.
    \begin{description}
        \item[Input Offset Voltage] ($V_{OS}$) will charge the feedback capacitor, causing the output to drift and eventually saturate.
        
        \item[Input Bias \& Offset Currents] ($I_B$ and $I_{OS}$) produce a voltage across the input resistor. A \SI{10}{nA} bias (typical) across a \SI{1}{M\ohm} resistor produces a \SI{10}{mV} DC offset, well beyond the $V_{OS}$ of most op-amps, and enough to cause saturation \textit{very} quickly.
        
        \item[Bandwidth \& Slew Rate] limit the step response of the integrator. Since both dual slope and sigma-delta conversions depend on the ability to quickly switch a reference signal, the step response is critical. However, high speed op-amps often have large DC offsets.
        
        \item[Input \& Output Voltage Range] Since the power supplies are limited to $\SI{\pm 5}{V}$, a rail-to-rail op-amp is preferable, though anything capable of going within \SI{500}{mV} of the supply rails should be adequate.
    \end{description}

    \item[Feedback Capacitor] properties can severely impact the accuracy and linearity of the integrator.
    \begin{description}
        \item[Stability] Temperature, DC bias, and a variety of other conditions can change the value of some capacitors well beyond their listed tolerance. An extreme example of these effects can be observed in large Class II ceramic capacitors commonly used for decoupling. On the other hand, Class I (C0G/NP0) ceramic capacitors are among the most stable available.
        
        \item[Leakage] (specified as insulation resistance or leakage current) is modeled as a large DC resistance in parallel with a capacitor. At low frequencies, this can result in DC offsets similar to those introduced by the op-amp.
        
        \item[\Gls{dielectric-absorption}] can result in an unavoidable time-dependent error.
    \end{description}
\end{description}

Initially, an autozero op-amp was considered; however, these tend to be noisy, and are typically limited to \SI{5}{V} operation.\cite{AoE} After multiple simulations, the Analog Devices LTC6244HV \cite{LTC6244} dual rail-to-rail CMOS op-amp was chosen for a variety of reasons. It has very low $I_B$ and $I_{OS}$ specifications (both under \SI{75}{pA}), a moderate $V_{OS}$ (100 to \SI{500}{\textmu V}). The bandwidth and slew rate are \SI{50}{MHz} and \SI{40}{V/\textmu s}, respectively; not particularly fast, but beyond the capabilities of most other op-amps with comparable input characteristics. The primary downside of the LTC6244HV is that it has a relatively high input capacitance, making it difficult to operate at maximum speed without running into stability issues.

The feedback capacitor is a \SI{180}{pF} Panasonic ECH-U1H181GX5, with a PPS (polyphenyline sulfide) film dielectric. Although PPS does not have the lowest dielectric absorption among film capacitors, it is low enough not to cause problems, and the Panasonic ECH-U series is one of the few options available in surface-mount packaging.

\subsection{Slope Selection} \label{design:intsum:int:slope}
A Vishay DG4052E \cite{DG405xE} dual 4:1 analog multiplexer allows for input resistor selection among four options: \SI{2}{k\ohm}, \SI{20}{k\ohm}, \SI{200}{k\ohm}, and \SI{2}{M\ohm}. As shown in Figure \ref{fig:design:intsum:int-ac-sim}, each successive resistor value results in a decade (\SI{20}{dB}) drop. Due to the fact that the on resistance ($R_{ON}$) of CMOS multiplexers can vary significantly over voltage, temperature, and between chips, a single DG4052E is shared by both integrators.

\begin{figure}
    \includegraphics[width=\textwidth]{int-ac-sim.png}
    \caption{AC analysis of the integrator using resistor options ranging from \SI{2}{k\ohm} (top) to \SI{2}{M\ohm} (bottom)}
    \label{fig:design:intsum:int-ac-sim}
\end{figure}

\subsection{Reset} \label{design:intsum:int:reset}
The reset circuit consists of an SPST analog switch, a resistor, and a small amount of glue logic. Originally, the Vishay DG467 switch was going to be used due to its low price. However, a pin numbering error resulted in a last-minute change to the functionally equivalent Vishay DG9421 \cite{DG9421} at the last minute. Upon further research, it was realized that the DG9421 was actually a better choice: it has a significantly lower leakage current and is capable of faster switching speeds.

Due to the fact that the DG9421 has a peak current rating of \SI{100}{mA} (\SI{1}{ms} pulse), a \SI{51}{\ohm} resistor was added in series with the switch terminals; this limits the current to just under \SI{100}{mA} assuming a worst-case \SI{5}{V} across the capacitor. Assuming a \SI{180}{pF} capacitor is used, the RC time constant is roughly \SI{10}{ns}. The simulation in Figure \ref{fig:design:intsum:reset-sim} demonstrates that the integrator can be considered completely reset after \SI{100}{ns}. However, this assumes that the switch turns on instantaneously. According to the datasheet, the DG9421 requires anywhere between 20 and \SI{60}{ns} to turn on or off.\footnote{Using a \SI{12}{V} power supply}\cite{DG9421} Thus, a full reset cycle should take around \SI{200}{ns} with a \SI{150}{ns} reset pulse.

\begin{figure}
    \includegraphics[width=\textwidth]{reset-sim.png}
    \caption{integrator reset simulation}
    \label{fig:design:intsum:reset-sim}
\end{figure}

Two active-low digital inputs can be used to reset the integrator:
\begin{enumerate}
    \item \texttt{\textoverline{RESET}} is intended to quickly reset the integrator between conversions, and will be connected directly to the FPGA fabric.
    
    \item \texttt{\textoverline{ENABLE}} forces the integrator to reset and disables the input switch (see Section \ref{design:intsum:input}). It will be connected to a weak pullup resistor and controlled by the FPGA's embedded hard processor.
\end{enumerate}

A 74LVC1G58 \cite{74LVC1G58} multi-function logic gate drives the switch, and is configured as an AND gate with one input inverted.This overrides \texttt{\textoverline{RESET}} when the integrator is disabled, and provides a Schmitt trigger input for the slow \texttt{\textoverline{ENABLE}} signal. A buffer was later added for \texttt{\textoverline{RESET}} in order to support \SI{2.5}{V} LVCMOS input levels.

\FloatBarrier
\section{Input Selection} \label{design:intsum:input}
\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{input-sch.pdf}
    \caption{input switch network schematic}
    \label{fig:design:intsum:input-sch}
\end{figure}

The input stage shown in Figure \ref{fig:design:intsum:input-sch} allows selection between direct integration and sigma-delta mode. A Vishay DG4052E is used to choose the input signal, and a DG4053E \cite{DG405xE} triple SPDT switch is used to change the mode. These switches have particularly low \gls{charge-injection} (\SI{0.3}{pC}), which helps minimize transient glitches.\cite{DG405xE,AoE}

The following control signals are defined:
\begin{description}
    \item[\texttt{\textoverline{ENABLE}}] sets a high impedance state for analog inputs and forces the integrator to reset (see Section \ref{design:intsum:int:reset})
    
    \item[\texttt{DELTA\_MODE}] enables the difference amplifier (see Section \ref{design:intsum:delta}) and passes its output to the integrator

    \item[\texttt{REF[1:0]}] selects between the input signal and reference voltages in integration mode
    \begin{description}[noitemsep]
        \item[00] $GNDA$: analog ground, potentially useful for DC offset compensation
        \item[01] $V_{IN}$: analog input signal
        \item[10] $V_{Ref+}$: negative reference voltage
        \item[11] $V_{Ref-}$: positive reference voltage
    \end{description}
\end{description}

In integration mode, a dual slope ADC can be implemented by selecting the $V_{IN}$ signal for a fixed amount of time, and then switching to either $V_{Ref+}$ or $V_{Ref-}$ based on the polarity of the integrated signal. In sigma-delta mode, the reference signal is subtracted from the input, with modulation achieved by toggling between $V_{Ref+}$ and $V_{Ref-}$. Since both the input and reference signals are passed through the same switch IC, switching transients are seen as common-mode in the differential amplifier.\footnote{Common-mode rejection and input impedance matching could have likely been improved by moving the buffer outside of the differential signal path, but doing so would require a new PCB revision.}

\FloatBarrier
\section{Differential Amplifier} \label{design:intsum:delta}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{delta-sch.pdf}
    \caption{difference amplifier schematic}
    \label{fig:design:intsum:delta:sch}
\end{figure}

The differential amplifier is relatively simple, consisting of only an LTC6244HV and a Vishay MORNTA1001AT5 \SI{1}{k\ohm} matched resistor network, as shown in Figure \ref{fig:design:intsum:delta:sch}. In order compensate for the high input capacitance of the LTC6244, a capacitor was added in parallel with the output and negative feedback resistor. Unfortunately, it is difficult to tell from a simulation what capacitor value to use; to mitigate this issue, a Knowles JR400 \cite{knowles-trimmers} trimmer was included, allowing the capacitance be adjusted manually between 8 and \SI{40}{pF}. Figure \ref{fig:intsum:delta:gain-sim} shows the differential gain over frequency using several capacitor values. According to simulations, a nominal value of \SI{33}{pF} provides a decent compromise between bandwidth and gain rolloff.

\begin{figure}
    \includegraphics[width=\textwidth]{delta-gain-sim.png}
    \caption{simulated differential gain with capacitor values of 8 (top), 22, and \SI{40}{pF} (bottom)}
    \label{fig:intsum:delta:gain-sim}
\end{figure}

\FloatBarrier
\section{Attenuator} \label{design:intsum:atten}
\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{atten-sch.pdf}
    \caption{attenuator schematic using an MDAC and inverting transimpedance amplifier}
    \label{fig:design:intsum:atten:sch}
\end{figure}

Originally, integrator slope selection was going to be provided through the use of a Resistive DAC (RDAC).\footnote{Also commonly known as a digital potentiometer} However, simulations demonstrated that no integrated RDACs could provide the necessary bandwidth and adjustment range. In addition, RDACs generally have poor tolerance,\footnote{Typically $\pm 20\%$ absolute resistance tolerance, with $\pm 1\%$ matching} and most are limited to \SI{5}{V} operation. Fortunately, there is another option: Multiplying DACs (MDACs).

An MDAC can be thought of as a digitally programmable attenuator, multiplying an analog input (reference) signal by a digital word through the use of a resistive ladder network and low-side switches; this produces an output current proportional to the input voltage.% Similar to the resistor string in an RDAC, the ladder network in an MDAC generally has very poor absolute tolerance. To mitigate this issue, most MDACs include an additional feedback pin that can be connected to an external amplifier.

As an attenuator, an MDAC has multiple benefits when compared to RDAC wired as a resistor divider:
\begin{itemize}[noitemsep]
    \item Constant impedance at the reference input across the full range of digital input words
    
    \item Linearity is maintained for input voltages well beyond the supply rails (positive and negative)\footnote{This is enabled through the use of low-side switching, as opposed to the transmission gate switches typically found in RDACs}

    \item Linearity is maintained at extreme values, no ``wiper resistance'' in series with the output
    
    \item A feedback resistor matched to the internal ladder network is provided for negative feedback
    
    \item Wider attenuation range for a given number of bits

    \item Higher resolutions are available
    
    \item Lower parasitic capacitance results in a wider bandwidth
\end{itemize}

As shown in Figure \ref{fig:design:intsum:atten:sch}, an Analog Devices AD5429 \cite{AD5429_5439_5449} dual 8-bit MDAC was used in conjunction with an LTC6244HV wired as a transimpedance amplifier (TIA) to produce a voltage output. Again, a dual package was shared by both \textit{IntSum} units in order to save space and improve component matching. It is worth noting that it was somewhat difficult to get the LTC6244HV to operate correctly due to its high input capacitance. As suggested in the datasheets for both the AD5429 and LTC6244, an external compensation capacitor was added.\cite{AD5429_5439_5449,LTC6244} A Knowles JR060 \cite{knowles-trimmers} trimmer allows the capacitance to be adjusted from 2 to \SI{6}{pF}. Figure \ref{fig:design:intsum:atten:sim} shows the simulated frequency response of the attenuator using a \SI{4}{pF} capacitor over the full digital input range.\footnote{A custom SPICE model was used for the MDAC, see Chapter \ref{design:sim:mdac}}

\begin{figure}
    \includegraphics[width=\textwidth]{atten-sim.png}
    \caption{simulated attenuation levels stepped by powers of 2 from 0 (bottom) to 255 (top)}
    \label{fig:design:intsum:atten:sim}
\end{figure}

\section{Conclusion} \label{design:intsum:end}
Despite the fact that it was behind schedule, the \textit{IntSum} stage design was completed successfully. A great deal was learned about precision op-amps, capacitor properties, and analog switches. In a future revision, several changes would be suggested:

\begin{itemize}[noitemsep]
    \item Remove the buffer from the input stage, and place it directly before the attenuator
    
    \item Perform lab tests in order to determine optimal compensation capacitor values so that the trimmers can be removed
    
    \item Investigate the possibility of removing the differential amplifier, replacing it with a summing node
\end{itemize}

%%% Local Variables:
%%% mode: luatex
%%% TeX-engine: luatex
%%% TeX-master: "Report"
%%% End:
