\graphicspath{{./img/Design_AnalogReferences/}}
\chapter{Voltage References} \label{design:ref}
ADC conversion results can only be as accurate as the reference voltage. In many cases, the reference voltage determines the input voltage range of the converter, as well as the voltage corresponding to \SI{1}{LSb} (least significant bit).

\section{Specification} \label{design:ref:spec}
\begin{itemize}[noitemsep]
    \item All reference voltages should be bipolar, centered at analog ground
    \item Several common reference voltages such as 2.048 and \SI{4.096}{V} should be available
    \item An adjustable reference would be useful, but is not required
\end{itemize}

\subsection{Constraints} \label{design:ref:spec:const}
One unusual aspect of this design in the requirement for \textit{bipolar} voltage references. Due to the fact that the positive and negative references do not need to be available simultaneously, it may be possible to use some clever switching to choose between positive and negative modes. It is also worth noting that there are two primary types of voltage references: series and shunt. Although series references tend to be slightly more accurate and provide better regulation, shunt references are more flexible.\cite{AoE}

\FloatBarrier
\section{Fixed References} \label{design:ref:fixed}
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.625\textwidth]{fixed-sch.pdf}
    \caption{fixed voltage references}
    \label{fig:design:ref:fixed:sch}
\end{figure}

After weighing the pros and cons of trying to switch a single reference between positive and negative voltages; however, this idea was quickly abandoned due to its complexity and concerns regarding load and switching transients. Thus, the simple ``brute force'' approach was taken when designing the fixed references, as shown in Figure \ref{fig:design:ref:fixed:sch}. The Analog Devices ADR5040B, ADR5041B, and ADR5044B shunt references are used generate bipolar reference voltages: $\pm 2.048$, $\pm 2.5$, and $\SI{\pm 4.096}{V}$, respectively.\cite{ADR504x} Since these devices are members of the same product family, one can expect them to have similar loading and transient responses.

\FloatBarrier
\section{Adjustable Reference} \label{design:ref:adj}
\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{adj-sch.pdf}
    \caption{MDAC-based bipolar adjustable voltage reference schematic}
    \label{fig:design:ref:adj:sch}
\end{figure}

The adjustable reference shown in Figure \ref{fig:design:ref:adj:sch} uses a Maxim Integrated MAX6034BEXR41+T \cite{MAX6034} \SI{4.096}{V} LDO (low-dropout) series voltage reference. An Analog Devices AD5443 \cite{AD5426_5432_5443} 12-bit MDAC configured to operate in bipolar mode; theoretically, this provides a resolution of \SI{2}{mV/LSb} with an output range of $\SI{\pm 4.096}{V}$, in accordance with Equation \ref{eqn:design:ref:adj:vdac}. Figure \ref{fig:design:ref:adj:mdac-sim} shows the simulated frequency response of the bipolar MDAC circuit; note that mirrored values have a $180\degree$ phase shift, indicating positive and negative polarity.

\begin{equation}
    V_{DAC} = \frac{4.096D}{2048} - 4.096
    \label{eqn:design:ref:adj:vdac}
\end{equation}

\begin{figure}
    \includegraphics[width=\textwidth]{mdac-bipolar-sim1.png}
    \includegraphics[width=\textwidth]{mdac-bipolar-sim2.png}
    \includegraphics[width=\textwidth]{mdac-bipolar-sim3.png}
    \includegraphics[width=\textwidth]{mdac-bipolar-sim4.png}
    \caption{Bipolar MDAC frequency response for various digital input words. Top to bottom: 1 and 4095 ($\SI{\pm 4.095}{V}$); 1024 and 3072 ($\SI{\pm 2.048}{V}$); 2016 and 2080 ($\SI{\pm 64}{mV}$); and 2048 (\SI{0}{V}).}
    \label{fig:design:ref:adj:mdac-sim}
\end{figure}

\FloatBarrier
\section{Output Selection} \label{design:ref:select}
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.4\textwidth]{select-sch.pdf}
    \caption{reference selection switch}
    \label{fig:design:ref:select:sch}
\end{figure}

As shown in Figure \ref{fig:design:ref:select:sch}, a DG4052E is used to select one of the three fixed references, or the adjustable reference. The adjustable reference is a special case. $V_{Ref-}$ is connected to analog ground in order to compensate for the switch resistance, and the polarity must be changed via the MDAC interface (see Chapter \ref{design:digital:mdac}).

\FloatBarrier
\section{Conclusion} \label{design:ref:end}
The voltage reference design went together fairly easily. In a future revision of the PCB, the MAX6034B should be replaced. It was initially chosen due to the fact that it was the only \SI{4.096}{V} series reference available in an SC70 package. However, there is sufficient space on the PCB for an SOT-23 package, which would greatly increase the available options.

%%% Local Variables:
%%% mode: luatex
%%% TeX-engine: luatex
%%% TeX-master: "Report"
%%% End: