\graphicspath{{./img/Design_Power/}}
\chapter{Power Supplies} \label{design:power}
According to the HSMC Specification, cards are provided with two power supplies: \SI{3.3}{V} and \SI{12}{V}.\cite{HSMC} While most of the logic, can be powered from \SI{3.3}{V}, the analog components require a low noise bipolar power supply. In addition, the MDACs require a ``floating'' supply in order to operate correctly.

\FloatBarrier
\section{Analog Bipolar Supply} \label{design:power:analog}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.625\textwidth]{analog-sch.pdf}
    \caption{$\SI{\pm 5}{V}$ analog power supply with virtual ground}
    \label{fig:design:power:analog:sch}
\end{figure}

The main power supply consists of two linear regulators, shown in Figure \ref{fig:design:power:analog:sch}. The Analog Devices LT1963A \cite{LT1963A} is an adjustable, \SI{1.5}{A}, low-noise LDO regulator with a fast transient response; this makes it perfectly suited for use with a mixed-signal analog design. Using standard 1\% resistor values of \SI{7.5}{k\ohm} and \SI{1}{k\ohm}, the output voltage is \SI{10.3075}{V} (Equation \ref{eqn:design:power:analog:LT1963A}), according to the equation provided in the datasheet.\cite{LT1963A}

\begin{equation}
    V_{OUT} = \left(\SI{1.21}{V}\right)
    \left(1 + \frac{\SI{7.5}{k\ohm}}{\SI{1}{k\ohm}}\right)
    + \left(\SI{3}{\micro A}\right) \left(\SI{7.5}{k\ohm}\right)
    = \SI{10.3075}{V}
    \label{eqn:design:power:analog:LT1963A}
\end{equation}

Several options were considered when attempting to generate a virtual ground voltage before settling on the Analog Devices LT1118-5 \cite{LT1118}, a fixed \SI{5}{V} \SI{800}{mA} LDO regulator. The LT1118 was chosen because it is one of the only linear regulators available that is capable of both sourcing \textit{and} sinking current; this is a critical feature, considering the fact that some fairly high return currents are expected. A green LED was added between the two regulators as a power indicator and to improve the transient response of the LT1118.\cite{LT1118}

\FloatBarrier
\section{Adjustable MDAC Supply} \label{design:power:mdac}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{mdac-sch.pdf}
    \caption{\SI{3.3}{V} MDAC power supply with an adjustable ground potential}
    \label{fig:design:power:mdac:sch}
\end{figure}

Initially, the MDACs were going to be powered from the main \SI{3.3}{V} logic supply, with the outputs biased at \SI{5}{V} (analog ground). Although this is within the absolute maximum ratings, further investigation revealed that linearity, gain, and offset errors all depend on this bias voltage.\cite{AD5426_5432_5443,AD5429_5439_5449} Due to the speed of the SPI interface used to configure the MDACs, it was undesirable to power them directly from the analog supplies.

To address these issues, a dedicated power supply was created for the MDACs using the adjustable variant of the LT1118 and a Microchip MIC5205 \cite{MIC5205} adjustable low-noise LDO regulator configured to produce a \SI{3.3}{V} output, as shown in Figure \ref{fig:design:power:mdac:sch}. A Microchip MCP4561 \cite{MCP4561} \SI{10}{k\ohm} nonvolatile 8-bit RDAC allows the bias voltage to be fine-tuned between 2.5 and \SI{5}{V}. Figure \ref{fig:design:power:mdac:rdac-sim} shows the range of usable RDAC settings; values below 128 or above 192 will be clamped.

\begin{figure}[h]
    \includegraphics[width=\textwidth]{mdac-rdac-sim.png}
    \caption{MDAC supply voltages vs. RDAC control word; each dot represents one RDAC setting}
    \label{fig:design:power:mdac:rdac-sim}
\end{figure}

\FloatBarrier
\section{5V Digital Supply} \label{design:power:digital}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.375\textwidth]{digital-sch.pdf}
    \caption{low power \SI{5}{V} logic supply}
    \label{fig:design:power:digital:sch}
\end{figure}

In order to prevent the MCP4561 from clamping the MDAC ground potential at \SI{3.3}{V}, a small \SI{5}{V} digital supply was added to the design. This also uses a MIC5205 LDO, as shown in Figure \ref{fig:design:power:digital:sch}.

\FloatBarrier
\section{Decoupling \& Bypass Capacitors} \label{design:power:cap}
The decoupling network was designed after extensively researching capacitor properties and choosing values and multiple simulations. Figure \ref{fig:design:power:cap:sim} shows a simulation the approximate source impedance near the MDACs. Note that extra bypass capacitors were added to filter out \SI{50}{MHz} switching noise from the SPI interface (see Chapter \ref{design:digital:mdac}). Figure \ref{fig:design:power:cap:bipolar-sim} plots the impedance of a decoupling network more typical for the analog power supplies.

The following capacitor technologies were used in the decoupling network:
\begin{description}
    \item[Solid Tantalum] capacitors were used for bulk decoupling. They offer excellent stability with respect to temperature, and tend to have lower ESR and ESL than their Aluminum Electrolytic counterparts.
    
    \item[Tantalum Polymer] capacitors are significantly more expensive than Solid Tantalum capacitors, but they have a significantly lower ESR, giving them a better frequency response. These were used for area decoupling.
    
    \item[Class II Ceramic] capacitors (such as X5R and X7R) change significantly with temperature and at different DC bias voltages. However, they are small, inexpensive, have an extremely low ESR, and are universally used for single-IC decoupling.
\end{description}

\begin{figure}
    \includegraphics[width=\textwidth]{cap-sim.png}
    \caption{MDAC decoupling network simulation}
    \label{fig:design:power:cap:sim}
\end{figure}

\begin{figure}
    \includegraphics[width=\textwidth]{cap-bipolar-sim.png}
    \caption{bipolar decoupling network simulation typical around analog components}
    \label{fig:design:power:cap:bipolar-sim}
\end{figure}
    

\FloatBarrier
\section{Conclusion} \label{design:power:end}
The power supplies went through multiple variations and revisions before the design presented in this chapter was finalized. In addition, a massive amount of time was spent sourcing and simulating the decoupling network. In a future revision, it may be worth performing lab tests to determine whether or not the adjustable MDAC power supply is actually beneficial; if not, the adjustable LT1118 can likely be replaced with the fixed \SI{5}{V} version.

%%% Local Variables:
%%% mode: luatex
%%% TeX-engine: luatex
%%% TeX-master: "Report"
%%% End:
