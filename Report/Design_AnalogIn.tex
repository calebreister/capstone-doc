\graphicspath{{./img/Design_AnalogIn/}}
\chapter{Input Stage} \label{design:in}
The goal of the input stage is to provide protection for all subsequent analog stages, and to interface with a variety of signal sources. Since the emphasis is on low-frequency, precision devices such as analog sensors, the input stage will be designed around an instrumentation amplifier.

\section{Specification} \label{design:in:spec}
The input stage will include the following capabilities:
\begin{itemize}
    \item Instrumentation amplifier, with a programmable gain
    \item Option to choose between DC and AC coupling
    \item Selectable input impedance with common termination options: \SI{50}{\ohm}, \SI{75}{\ohm}, \SI{100}{\ohm}, \SI{150}{\ohm}, and high impedance
\end{itemize}

\subsection{Register Map} \label{design:in:spec:reg}
\begin{bytefield}[endianness=big, bitwidth=0.125\linewidth]{8}
    \bitheader{0-7} \\
    % Bit definitions
    \bitbox{1}{\ubit} & % unused
    \bitbox{1}{\textoverline{FAULT}} &
    \bitbox{1}{AC\_MODE} &
    \bitbox{1}{ZIN\_EN} &
    \bitbox{2}{ZIN[1:0]} &
    \bitbox{2}{GAIN[1:0]} \\
    % Read/write: default value
    \bitboxes*[]{1}{{R: 0} {R: U} {R/W: 0} {R/W: 0}} &
    \bitbox[]{2}{R/W: 00} &
    \bitbox[]{2}{R/W: 00} 
\end{bytefield}

\begin{description}
    \item[\texttt{\textoverline{FAULT}}] (optional) reads 0 when a fault condition is detected at the input (see Section \ref{design:in:ac})
    \item[\texttt{AC\_MODE}] switches the input stage between DC and AC coupling modes
    \begin{description}[noitemsep]
        \item[0] DC coupling (default)
        \item[1] AC coupling
    \end{description}
    \item[\texttt{ZIN\_EN}] enables the variable input impedance stage preceding the instrumentation amplifier
    \begin{description}[noitemsep]
        \item[0] input is high impedance, \verb|ZIN[1:0]| bits are unused (default)
        \item[1] input impedance determined by \verb|ZIN[1:0|
    \end{description}
    \item[\texttt{ZIN[1:0]}] sets the input impedance
    \begin{description}[noitemsep]
        \item[00] \SI{50}{\ohm} (default)
        \item[01] \SI{75}{\ohm}
        \item[10] \SI{100}{\ohm}
        \item[11] \SI{150}{\ohm}
    \end{description}
    \item[\texttt{GAIN[1:0]}] sets the gain of the instrumentation amplifier
    \begin{description}[noitemsep]
        \item[00] $G = 1$
        \item[01] $G = 10$
        \item[10] $G = 100$
        \item[11] $G = 1000$ (optional)
    \end{description}
\end{description}

\subsection{Constraints} \label{design:in:spec:const}
The primary design constraint for the input stage is that all components must be able to withstand over- and under-voltage conditions. This severely limits analog switch options, as most protection diodes can introduce DC offsets at the output due to reverse leakage. Since the goal is to develop a low-frequency, high-precision analog front end architecture, component bandwidths do not matter a great deal.

\FloatBarrier
\section{Instrumentation Amplifier} \label{design:in:inamp}
The Analog Devices AD8253 \cite{AD8253} was chosen to serve as the instrumentation amplifier for a variety of reasons. It is one of the few available instrumentation amplifiers with pin-programmable gain of 1, 10, 100, and 1000; which will simplify the PCB design. It has a \SI{10}{MHz} unity gain bandwidth, and is available in a hand-solderable MSOP-10 package. The primary downside to the AD8253 is that its input and output common mode ranges are fairly limited, especially with $\SI{\pm 5}{V}$ power supplies.

Figure \ref{fig:design:in:inamp:inamp} shows how the AD8253 will be used in the input stage. A pair of matched resistors connected to the inputs of the amplifier should provide adequate input protection without contributing a significant amount of input noise or DC offsets. A value of \SI{1.3}{k\ohm} was chosen based on the recommended value for the AD8250 (a close relative of the AD8253). \cite{inamp-guide}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth]{inamp.pdf}
    \caption{instrumentation amplifier schematic}
    \label{fig:design:in:inamp:inamp}
\end{figure}

The gain of the AD8253 is set using the \texttt{A[1:0]} digital inputs. Note, however, that these are are referenced to the \texttt{DGND} pin, which must be at least \SI{4.25}{V} above the negative supply. This caused a small issue, since the ``negative'' supply in this project is actually the FPGA ground plane and ``analog ground'' actually at \SI{5}{V}. Since \texttt{DGND} needed to be above the negative supply, it was connected to the same \SI{5}{V} supply used as the analog reference. This should be acceptable, since the gain will only be switched between conversions.\footnote{If necessary, a dedicated digital \SI{5}{V} power supply may be created later.} After researching various isolation techniques, it was determined through simulation that the simple level shifting circuit shown in Figure \ref{fig:design:in:inamp:logic} would provide acceptable performance and be much cheaper.

\begin{figure}
    \centering
    \includegraphics[width=0.35\textwidth]{inamp-logic.pdf}
    \caption{level shifting circuit used to drive the gain pins (\texttt{A[1:0]}) on the AD8253}
    \label{fig:design:in:inamp:logic}
\end{figure}

\FloatBarrier
\section{AC Coupling} \label{design:in:ac}
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{ac.pdf}
    \caption{AC coupling switch and input protection circuit}
    \label{fig:design:in:ac}
\end{figure}

Some sensors require a DC bias well beyond the available power supply range. To mitigate this issue, a mechanical relay will be used to allow the input signal to be AC-coupled, as shown in Figure \ref{fig:design:in:ac}. Following the relay, an ADG5462F \cite{ADG5462F} provides additional protection by disconnecting the subsequent stages when it detects a fault (under- or over-voltage) condition. The ADG5462F is capable of blocking $\SI{\pm 55}{V}$ (even when unpowered) on four channels, and includes an open-drain \texttt{\textoverline{FAULT}} output that to notify external devices that the fault occurred.

\FloatBarrier
\subsection{Design Iterations} \label{design:in:ac:old}
Several versions of the AC coupling circuit were developed before settling on the version shown in Figure \ref{fig:design:in:ac}. Figure \ref{fig:design:in:ac-old} shows one such iteration that uses two relays with no input protection; which was eventually abandoned due to concerns about PCB space constraints.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{ac-old.pdf}
    \caption{original AC coupling switch circuit (no input protection)}
    \label{fig:design:in:ac-old}
\end{figure}

\FloatBarrier
\section{Adjustable Input Impedance} \label{design:in:zin}
An analog multiplexer will be used to select between the five input impedance options, as shown in Figure \ref{fig:design:in:zin}. The Analog Devices ADG1609 \cite{ADG1609} was chosen primarily for its low on resistance. Resistor values were chosen to be \SI{15}{\ohm} below the desired input resistances in order to compensate for the combined on resistances of the ADG5462F (\SI{10}{\ohm}), ADG1636 (\SI{1}{\ohm}), and ADG1609 (\SI{4.5}{\ohm}).

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{zin.pdf}
    \caption{input impedance selection circuit}
    \label{fig:design:in:zin}
\end{figure}

\FloatBarrier
\subsection{Design Iterations} \label{design:in:zin:old}
Several alternate designs for the input impedance selection circuit were considered in an attempt to reduce the on resistance of the analog switches. The ``brute force'' technique shown in Figure \ref{fig:design:in:zin:old:relay} uses three DPDT relays (with two of them wired in an SPST configuration) to replicate the functionality of the analog switch in Figure \ref{fig:design:in:zin}. Although it has negligible on resistance, relays require significantly more board space and consume a lot of power when energized.

Figure \ref{fig:design:in:zin:old:switch} implements a similar circuit using SPST analog switches. However, this technique would have required additional glue logic, as shown in Figure \ref{fig:design:in:zin:old:logic}.

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{zin-relay-old.pdf}
    \caption{input impedance selection using DPDT relays}
    \label{fig:design:in:zin:old:relay}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{zin-switch-old.pdf}
    \caption{input impedance selection using SPST analog switches}
    \label{fig:design:in:zin:old:switch}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{zin-logic-old.pdf}
    \caption{control logic for the SPST switches implemented using multi-function gates \cite{74LVC1G99}}
    \label{fig:design:in:zin:old:logic}
\end{figure}

\section{Revisions} \label{design:in:rev}
\begin{itemize}
    \item The Kemet EB2-12NU \cite{EA2_EB2} \SI{12}{V}, surface-mount relay used in the initial design was replaced with the cheaper EA2-5NU, a \SI{5}{V}, through-hole variant from the same product family. This was done to avoid routing the noisy \SI{12}{V} power supply across the PCB.
    
    \item The 2N7002 discrete N-Channel MOSFETs used for level translation and relay control were replaced with the BSS138 \cite{BSS138}, which has improved characteristics at low gate voltages.

    \item Due to a variety of technical and logistical concerns, the adjustable input impedance circuit was removed from the final design entirely.
\end{itemize}

%%% Local Variables:
%%% mode: luatex
%%% TeX-engine: luatex
%%% TeX-master: "Report"
%%% End:
