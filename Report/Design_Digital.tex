\graphicspath{{./img/Design_Digital/}}
\chapter{Digital Interfacing} \label{design:digital}
Although all of the conversion logic will be implemented in the FPGA, there is some interfacing and translation logic on the PCB. Basic configuration can be performed via I2C, various control signals are connected to the FPGA, and the MDACs are configured via an isolated \SI{50}{MHz} SPI interface. It is worth noting that although there is a \SI{3.3}{V} power supply, the FPGA has \SI{2.5}{V} LVCMOS outputs and \SI{3.3}{V}-tolerant inputs.

\section{Comparator} \label{design:digital:comp}
\begin{figure}[h]
    \centering
    \includegraphics{comp-sch.pdf}
    \caption{comparator with LVDS driver}
    \label{fig:design:digital:comp:sch}
\end{figure}

The entire analog signal chain terminates at a single comparator: the Analog Devices LT1719 \cite{LT1719}, shown in Figure \ref{fig:design:digital:comp:sch}. The LT1719 was chosen due to its high speed and dual power supply support. To reduce switching noise and ground currents, the output is connected to a Texas Instruments DS90LV011A \cite{DS90LV011A} LVDS driver.

\section{I2C Bus} \label{design:digital:i2c}
\begin{figure}[h]
    \centering
    \includegraphics{i2c-pullup-sch.pdf}
    \caption{I2C pullup resistors and ESD protection diodes}
    \label{fig:design:digital:i2c:pullup-sch}
\end{figure}

The I2C bus includes three devices, listed in Table \ref{tab:design:digital:i2c:addr}. An ON Semiconductor CAT24C256 \cite{CAT24C256} \SI{32}{KiB} EEPROM (see Figure \ref{fig:design:digital:i2c:eeprom-sch}) is included to store information about the PCB, configuration defaults, or perhaps a Linux device tree overlay. All devices on the bus support \SI{400}{kHz} (Fast-Mode) I2C. Finally, a simple 3.3 to \SI{5}{V} I2C level shifter was implemented to interface with the MCP4561 RDAC using BSS138 MOSFETS,\cite{BSS138} as shown in Figure \ref{fig:design:digital:i2c:rdac-sch}.

\begin{table}[h]
    \centering
    \caption{I2C device list}
    \begin{tabular}{lcccl}
      Device    & Voltage     & I2C Address         & Description \\ \hline
      CAT24C256 & \SI{3.3}{V} & \texttt{1010\,000X} & \SI{32}{KiB} EEPROM \\
      MCP23017  & \SI{3.3}{V} & \texttt{0100\,000X} & GPIO Expander \\
      MCP4561   & \SI{5}{V}   & \texttt{0101\,111X} & Nonvolatile RDAC
    \end{tabular}
    \label{tab:design:digital:i2c:addr}
\end{table}

\begin{figure}[h]
    \centering
    \includegraphics{i2c-eeprom-sch.pdf}
    \caption{card info EEPROM}
    \label{fig:design:digital:i2c:eeprom-sch}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics{i2c-rdac-sch.pdf}
    \caption{RDAC with I2C level shifter}
    \label{fig:design:digital:i2c:rdac-sch}
\end{figure}

\FloatBarrier
\subsection{GPIO} \label{design:digital:i2c:gpio}
A Microchip MCP23017 \cite{MCP23017} GPIO expander is used to perform basic configuration, as shown in Figure \ref{fig:design:digital:i2c:gpio-sch}. This makes it easy to enable and disable modules, select the reference voltage, and perform a variety of other tasks using either the FPGA or an external controller.

\subsubsection{GPIO Port Map}
\begin{bytefield}[endianness=big, bitwidth=0.125\linewidth]{8}
    \bitheader{0-7} \\
    % Port B
    \wordbox{1}{Port B} \\
    \bitbox{1}{\textoverline{ACTIVE}} &
    \bitbox{2}{REF[1:0]} &
    \bitbox{2}{IS.SLOPE[1:0]} &
    \bitbox{1}{IS.DELTA} &
    \bitbox{1}{IS2.\textoverline{EN}} &
    \bitbox{1}{IS1.\textoverline{EN}} \\
    
    % Port A
    \wordbox{1}{Port A} \\
    \bitbox{1}{IN.\textoverline{FAULT}} &
    \bitbox{1}{IN.AC} &
    \bitbox{3}{\ubit} &
    \bitbox{2}{IN.GAIN[1:0]} &
    \bitbox{1}{\textoverline{PLUG}}
\end{bytefield}

\begin{figure}[h!]
    \centering
    \includegraphics{i2c-gpio-sch.pdf}
    \caption{I2C GPIO interface}
    \label{fig:design:digital:i2c:gpio-sch}
\end{figure}

\FloatBarrier
\begin{description}
    \item[\textoverline{ACTIVE}] (out, external pullup): user-defined activity indicator LED

    \item[{REF[1:0]}] (out): global voltage reference selection (see Chapter \ref{design:ref})
    \begin{description}[noitemsep]
        \item[00] $\SI{\pm 2.048}{V}$
        \item[01] $\SI{\pm 2.5}{V}$
        \item[10] $\SI{\pm 4.096}{V}$
        \item[11] adjustable
    \end{description}

    \item[{IS.SLOPE[1:0]}] (out): global IntSum slope selection (see Chapter \ref{design:intsum:int:slope})
    \begin{description}[noitemsep]
        \item[00] $\SI{\pm 2.048}{V}$
        \item[01] $\SI{\pm 2.5}{V}$
        \item[10] $\SI{\pm 4.096}{V}$
        \item[11] adjustable
    \end{description}

    \item[IS.DELTA] (out): global IntSum sigma-delta mode (see Chapter \ref{design:intsum:input})

    \item[IS2.\textoverline{EN} and IS1.\textoverline{EN}] (out, external pullup): enable signals for IntSum1 and IntSum2 (see Chapter \ref{design:intsum:int:reset})

    \item[IN.\textoverline{FAULT}] (in, internal pullup): input fault flag (see Chapter \ref{design:in})

    \item[IN.AC] (out, external pulldown): input stage AC coupling mode (see Chapter \ref{design:in})

    \item[{IN.GAIN[1:0]}] (out): input stage instrumentation amplifier gain selection (see Chapter \ref{design:in})

    \item[\textoverline{PLUG}] (in, external pullup): analog input plug insertion flag
\end{description}

\section{MDAC SPI Interface} \label{design:digital:mdac}
\begin{figure}
    \centering
    \includegraphics{mdac-sch.pdf}
    \caption{MDAC SPI interface}
    \label{fig:design:digital:mdac:sch}
\end{figure}

The MDACs (see Chapters \ref{design:intsum:atten} and \ref{design:ref:adj}) are controlled via a shared SPI interface, and are capable of operating at up to \SI{50}{Mb/s} with readback disabled.\cite{AD5426_5432_5443,AD5429_5439_5449} Since both MDACs must be placed near precision analog circuits, the SPI signals are translated to LVDS for transmission to and from the FPGA, as shown in Figure \ref{fig:design:digital:mdac:sch}. A Texas Instruments DSLVDS1048 \cite{DSLVDS1048} receiver and DSLVDS1047 \cite{DSLVDS1047} driver are used to perform LVDS-to-LVCMOS and LVCMOS-to-LVDS translations in order to minimize single-ended signal lengths. A Silicon Labs Si8662 \cite{Si866x} digital isolator shifts the ground potential of the signals to the MDAC bias voltage. Due to the fact that a \SI{50}{MHz} clock period is on the same order of magnitude as the round-trip propagation delay, of the level translation ICs, the SCLK signal is routed back through the isolator and to the FPGA. This can be used for phase compensation when reading data from the MDACs.

\section{FPGA I/O Pins} \label{design:digital:fpga}
\begin{description}
    \item[SCL] (in/out, \SI{3.3}{V} LVTTL, external pullup): I2C clock, \SI{400}{kHz} max.

    \item[SDA] (in/out, \SI{3.3}{V} LVTTL, external pullup): I2C data
    
    \item[\textoverline{INT}] (in, \SI{3.3}{V} LVCMOS, external pullup): MCP23017 interrupt signal

    \item[IntSum1.\textoverline{RESET} and IntSum2.\textoverline{RESET}] (out, \SI{2.5}{V} LVCMOS): integrator reset signals

    \item[{IntSum1.REF[1:0]}] (out, \SI{2.5}{V} LVCMOS): IntSum1 input/reference selection
    \begin{description}[noitemsep]
        \item[00] analog ground
        \item[01] instrumentation amplifier output
        \item[10] $V_{Ref-}$
        \item[11] $V_{Ref+}$
    \end{description}

    \item[{IntSum2.REF[1:0]}] (out, \SI{2.5}{V} LVCMOS): IntSum2 input/reference selection
    \begin{description}[noitemsep]
        \item[00] analog ground
        \item[01] IntSum1 output
        \item[10] $V_{Ref-}$
        \item[11] $V_{Ref+}$
    \end{description}

    \item[COMP\pm] (in, LVDS): comparator output

    \item[\textoverline{CS\_REF}\pm] (out, LVDS): SPI chip select for the adjustable reference MDAC

    \item[\textoverline{CS\_SLOPE}\pm] (out, LVDS): SPI chip select for the IntSum attenuation MDAC
   
    \item[SCLK\_W\pm] (out, LVDS): SPI write clock

    \item[SCLK\_R\pm] (in, LVDS): SPI read clock (useful for phase compensation)

    \item[MOSI\pm] (out, LVDS): SPI master out, slave in

    \item[MISO\pm] (in, LVDS): SPI master in, slave out
\end{description}

\section{Conclusion} \label{design:digital:end}
Although it should work, the digital interface could benefit from several improvements:
\begin{itemize}[noitemsep]
    \item The MCP23017 could (and should!) be replaced entirely with a GPIO IP core controlled by the hard processor in the FPGA.
    
    \item Although it may be useful for debugging, the MDAC readback feature is not required. It may be better to remove the SPI read signals altogether.

    \item Several LVDS receivers with an extended common-mode input range exist. It may be possible to remove the digital isolator, resulting in reduced emissions and a smaller propagation delay.
\end{itemize}

%%% Local Variables:
%%% mode: luatex
%%% TeX-engine: luatex
%%% TeX-master: "Report"
%%% End:
